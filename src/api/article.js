import request from '@/utils/request'
// 获取文章列表
export const articles = (channelid, timestamp) => {
  return request('get', '/app/v1_1/articles', { channel_id: channelid, timestamp, with_top: 1 })
}
// 发送举报
export const report = (target, type) => request('post', '/app/v1_0/article/reports', { target, type })
// 获取文章详情
export const articleDetail = (id) => request('get', `/app/v1_0/articles/${id}`)
// 获取评论列表
export const commentList = (type, source, offset) => request('get', '/app/v1_0/comments', { type, source, offset, limit: 10 })
// 发表评论
export const comment = (target, content, artid) => request('post', '/app/v1_0/comments', { target, content, art_id: artid })
// 点赞
export const giveLike = (target) => request('post', '/app/v1_0/article/likings', { target })
// 取消点赞
export const cancelLike = (target) => request('delete', `/app/v1_0/article/likings/${target}`)
// 不喜欢
export const dislike = (target) => request('post', '/app/v1_0/article/dislikes', { target })
// 取消不喜欢
export const cancelDislike = (target) => request('delete', `/app/v1_0/article/dislikes/${target}`)
// 拉黑作者
export const black = (target) => request('post', '/app/v1_0/user/blacklists', { target })
// 取消拉黑
export const cancelBlack = (target) => request('delete', `/app/v1_0/user/blacklists/${target}`)
// 收藏文章
export const collectArticle = (target) => request('post', '/app/v1_0/article/collections', { target })
// 取消收藏
export const cancelCollect = (target) => request('delete', `/app/v1_0/article/collections/${target}`)
