const KEY = 'app-user'

export default {
  getUser() {
    return JSON.parse(window.localStorage.getItem(KEY) || '{}')
  },
  setUser(user) {
    return window.localStorage.setItem(KEY, JSON.stringify(user))
  },
  delUser() {
    return window.localStorage.removeItem(KEY)
  }
}
