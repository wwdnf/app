import Vue from 'vue'
import Vuex from 'vuex'

import auth from '@/utils/auth'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 使用本地存储来获取token 防止手动刷新 数据销毁
    user: auth.getUser(),
    photo: null
  },
  mutations: {
    setUser(state, user) {
      state.user = user
      auth.setUser(user)
    },
    delUser(state) {
      state.user = {}
      auth.delUser()
    },
    setPhoto(state, photo) {
      state.photo = photo
    }
  },
  actions: {
  },
  modules: {
  }
})
