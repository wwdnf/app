import request from '@/utils/request'

// 获取搜索联想
export const suggest = q =>
  request('get', '/app/v1_0/suggestion', { q })
// 删除搜索搜索历史
export const delHistory = () =>
  request('delete', '/app/v1_0/search/histories')
// 获取搜索结果
export const getSearch = ({ q, page, perpage }) =>
  request('get', '/app/v1_0/search', { q, page, per_page: perpage })
// 获取用户搜索历史
export const searchHistory = () =>
  request('get', '/app/v1_0/search/histories')
