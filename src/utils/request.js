import axios from 'axios'
import JSONBIGINT from 'json-bigint'
import store from '@/store'
import router from '@/router'
const instance = axios.create({
  baseURL: 'http://ttapi.research.itcast.cn',
  transformResponse: [
    (response) => {
      try {
        return JSONBIGINT.parse(response)
      } catch (error) {
        return response
      }
    }
  ]
})
// 请求拦截器
instance.interceptors.request.use(
  config => {
    if (store.state.user.token) {
      config.headers.Authorization = `Bearer ${store.state.user.token}`
    }
    return config
  },
  err => Promise.reject(err)
)
// 响应拦截器
instance.interceptors.response.use(
  response => response.data,
  async err => {
    const loginConfig = { path: '/login', query: { redirectUrl: router.currentRoute.path } }
    try {
      if (err.response && err.response.status === 401) {
        // 记录这次请求路由地址 再刷新token后,重定向到该路由
        const { user } = store.state
        // 返回401有2种情况 1. 没有登录 2. token过期
        // 如果没有登录 跳转到登录页面进行登录
        if (!user.token || !user.refresh_token) {
          router.push(loginConfig)
          return Promise.reject(err)
        }
        // 如果token过期 则发送请求 刷新token
        const res = await axios({
          url: 'http://ttapi.research.itcast.cn/app/v1_0/authorizations',
          method: 'put',
          headers: {
            Authorization: `Bearer ${user.refresh_token}`
          }
        })
        // 将新的token储存
        store.commit('setUser', {
          token: res.data.data.token,
          refresh_token: user.refresh_token
        })
        // 重新发送请求
        return instance(err.config)
      }
    } catch (e) {
      router.push(loginConfig)
      return Promise.reject(e)
    }

    return Promise.reject(err)
  }
)

export default (method, url, data) => {
  return instance({
    method,
    url,
    [method.toUpperCase() === 'GET' ? 'params' : 'data']: data
  })
}
