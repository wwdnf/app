import request from '@/utils/request'
import router from '@/router'
// 登录
export const userLogin = loginForm => request('post', '/app/v1_0/authorizations', loginForm)

// 获取用户信息
export const getUserInfo = () => request('get', '/app/v1_0/user')

// 获取用户关注列表
export const getAttentions = (page, perpage) =>
  request('get', '/app/v1_0/user/followings', { page, per_page: perpage })

// 获取用户粉丝列表
export const getFollowers = (page, perpage) =>
  request('get', '/app/v1_0/user/followers', { page, per_page: perpage })

// 获取用户收藏列表
export const getCollections = (page, perpage) =>
  request('get', '/app/v1_0/article/collections', { page, per_page: perpage })

// 获取用户阅读历史
export const getReadHistory = (page, perpage) =>
  request('get', '/app/v1_0/user/histories', { page, per_page: perpage })

// 获取当前用户发布的文章
export const getUserArticles = (page, perpage) =>
  request('get', '/app/v1_0/user/articles', { page, per_page: perpage })

// 获取用户个人资料
export const getUserProfile = () => request('get', '/app/v1_0/user/profile')

// 编辑用户个人资料
export const editUserProfile = (data) => request('patch', '/app/v1_0/user/profile', data)

// 编辑用户个人头像
export const editUserPhoto = (photo) => {
  const formData = new FormData()
  formData.append('photo', photo)
  return request('patch', '/app/v1_0/user/photo', formData)
}

// 关注用户
export const attent = (target) => request('post', '/app/v1_0/user/followings', { target })

// 取消关注
export const cancelAttent = (target) => request('delete', `/app/v1_0/user/followings/${target}`)

// 获取指定用户资料
export const getInfo = (target) => request('get', `/app/v1_0/users/${target}`)

// 获取指定用户文字
export const getOtherArticles = (target, page, perpage) =>
  request('get', `/app/v1_0/users/${target}/articles`, { page, per_page: perpage })
