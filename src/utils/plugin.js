import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import 'dayjs/locale/zh-cn'
dayjs.extend(relativeTime)
// 延迟1s
const sleep = () => new Promise((resolve, reject) => {
  window.setTimeout(() => {
    resolve()
  }, 1000)
})
// 时间日期过滤
const relTime = (value) => {
  return dayjs().locale('zh-cn').from(value)
}
export default {
  install(Vue) {
    Vue.prototype.$sleep = sleep
    Vue.filter('relTime', relTime)
  }
}
