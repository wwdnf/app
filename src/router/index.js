import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

// import Layout from '@/views/Layout'
const Layout = () => import('@/views/Layout')
const Home = () => import('@/views/Home')
const Article = () => import('@/views/Home/Article')
const Question = () => import('@/views/Question')
const Video = () => import('@/views/Video')

const User = () => import('@/views/User')
const Profile = () => import('@/views/User/Profile')
const Chat = () => import('@/views/User/Chat')
const Login = () => import('@/views/User/Login')
const Black = () => import('@/views/User/Black')
const Collect = () => import('@/views/User/Collect')
const Works = () => import('@/views/User/Works')
const History = () => import('@/views/User/History')
const Detail = () => import('@/views/User/Detail')
const Search = () => import('@/views/Search/Search')
const Result = () => import('@/views/Search/Result')
// import Home from '@/views/Home'
// import Article from '@/views/Home/Article'

// import Question from '@/views/Question'
// import Video from '@/views/Video'

// import User from '@/views/User'
// import Profile from '@/views/User/Profile'
// import Chat from '@/views/User/Chat'
// import Login from '@/views/User/Login'
// import Black from '@/views/User/Black'
// import Collect from '@/views/User/Collect'
// import Works from '@/views/User/Works'
// import History from '@/views/User/History'
// import Detail from '@/views/User/Detail'

// import Search from '@/views/Search/Search'
// import Result from '@/views/Search/Result'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Layout,
    children: [
      { path: '/', name: 'home', component: Home, meta: { isKeepAlive: true } },
      { path: '/question', name: 'question', component: Question },
      { path: '/video', name: 'video', component: Video },
      { path: '/user', name: 'user', component: User }
    ]
  },
  { path: '/login', component: Login },
  { path: '/user/profile', component: Profile },
  { path: '/user/chat', component: Chat },
  { path: '/user/black', component: Black },
  { path: '/user/collect', component: Collect },
  { path: '/user/works', component: Works, meta: { isKeepAlive: true } },
  { path: '/user/history', component: History },
  { path: '/user/detail/:id', component: Detail },
  { path: '/search', component: Search },
  { path: '/search/result', component: Result, meta: { isKeepAlive: true } },
  { path: '/article/:id', component: Article, meta: { isKeepAlive: true } }

]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // 如果用户没有登录并且想访问个人中心相关的内容 跳转登录页
  if (to.path.startsWith('/user') && !store.state.user.token) return next('/login')
  next()
})
export default router
