import request from '@/utils/request'
import store from '@/store'
const KEY = 'app-channel'
// 请求频道列表
// 如果登录了,请求用户的频道列表
//  如果没有登录,本地也没有存储,请求默认频道列表,并且本地存储
// 如果没有登录,本地存储的默认频道列表
export const channels = () => {
  // 每次进入函数先获取登录状态
  const { user } = store.state
  // 登录了
  // 要使用pormise包装
  return new Promise(async (resolve, reject) => {
    if (user.token) {
      const data = await request('get', '/app/v1_0/user/channels')
      resolve(data)
    } else {
      const localChannel = JSON.parse(window.localStorage.getItem(KEY) || '[]')
      // 没登录 本地没存储
      if (!localChannel.length) {
        // 这里必须使用await 否则可能数据还没有 就去存储了
        const { data } = await request('get', '/app/v1_0/user/channels')
        window.localStorage.setItem(KEY, JSON.stringify(data.channels))
        resolve({ data })
      }
      // 没登录 本地存储
      resolve({ data: { channels: localChannel } })
    }
  })
}
// 获取全部频道
export const allChannels = () => request('get', '/app/v1_0/channels')
// 添加用户的频道
export const addChannel = (channels) => {
  const newchannels = channels.map((item, index) => {
    item.seq = index
    return item
  })
  newchannels.shift()
  return request('put', '/app/v1_0/user/channels', { channels: newchannels })
}
// 删除用户的频道
export const removeChannel = (target) => request('delete', `/app/v1_0/user/channels/${target}`)
